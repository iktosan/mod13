#include <iostream>
#include <cmath>

class Vector
{
private:
	double x = 0;
	double y = 0;
	double z = 0;

public:
	Vector()
	{}

	Vector(double _x, double _y, double _z): x(_x),y(_y),z(_z)
	{}

	void Show()
	{
		std::cout << x << ' ' << y << ' ' << z;
	}

	void Length()
	{
		std::cout << sqrt(x * x + y * y + z * z);
	}

};

int main()
{
	Vector a(3, 4, 5);
	a.Length();

}